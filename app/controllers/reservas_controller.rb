class ReservasController < ApplicationController
  before_action :set_reserva, only: [:show, :edit, :update, :destroy]

  def index
    @reservas = Reserva.all
  end

  def listar
    @reservas = Reserva.select(:nombreCliente, :estado_id).order(:nombreCliente)
  end

  def show
  end

  def new
    @reserva = Reserva.new
  end

  def

  def edit
  end

  def create
    @reserva = Reserva.new(reserva_params)

    stateRoom = Estado.find_by(id: @reserva.estado_id)
    begin
      if(stateRoom.estado == 0)
        stateRoom.update(estado: 1)

        respond_to do |format|
          if @reserva.save
            format.html { redirect_to @reserva, notice: 'Reservacion realizada con exito' }
            format.json { render :show, status: :created, location: @reserva }
          else
            format.html { redirect_to stateRoom, notice: 'Reservacion no registrada, validar campos' }
            format.json { render :show, status: :created, location: stateRoom }
          end
        end
      else
        respond_to do |format|
          format.html { redirect_to stateRoom, notice: 'Habitacion ocupada, Reservacion no registrada' }
          format.json { render :show, status: :created, location: stateRoom }
        end
      end
    rescue
      render 'new'
    end
  end

  def destroy
    @reserva.destroy
    respond_to do |format|
      format.html { redirect_to reservas_url, notice: 'Reservacion eliminada' }
      format.json { head :no_content }
    end
  end

  private
    def set_reserva
      @reserva = Reserva.find(params[:id])
    end

    def reserva_params
      params.require(:reserva).permit(:estado_id, :nombreCliente, :cedulaCliente, :celular, :correo, :numeroDias, :totalPagar)
    end
  
end
