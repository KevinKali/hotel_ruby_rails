class EstadosController < ApplicationController
  before_action :set_estado, only: [:show, :edit, :update, :destroy]

  def liberar
  end

  def muestra
    @estados = Estado.order(:id)
  end

  def mostrarDisp
    @estados = Estado.where(estado: 0).order(:id)
  end

  def liberaH
    x = params[:nH]
    stateRoom = Estado.find_by(id: x)
    begin
      value = stateRoom.estado == 0 ? 1 : 0
      if value == 0
        stateRoom.update(estado: value)

        reserva = Reserva.find_by(estado_id: x)
        reserva.destroy

        respond_to do |format|
          format.html { redirect_to stateRoom, notice: 'Liberar reservacion, accion realizada con exito' }
          format.json { render :show, status: :created, location: stateRoom }
        end
      else
        respond_to do |format|
          format.html { redirect_to stateRoom, notice: 'Liberar reservacion, la habitacion esta disponible no hace falta liberar' }
          format.json { render :show, status: :created, location: stateRoom }
        end
      end
    rescue
      render "liberar"
    end  
  end

  def fotos
  end

  # GET /estados/1
  # GET /estados/1.json
  def show
  end

  # GET /estados/new
  def new
    @estado = Estado.new
  end

  # GET /estados/1/edit
  def edit
  end

  # POST /estados
  # POST /estados.json
  def create
    @estado = Estado.new(estado_params)

    respond_to do |format|
      if @estado.save
        format.html { redirect_to @estado, notice: 'Estado was successfully created.' }
        format.json { render :show, status: :created, location: @estado }
      else
        format.html { render :new }
        format.json { render json: @estado.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /estados/1
  # DELETE /estados/1.json
  def destroy
    @estado.destroy
    respond_to do |format|
      format.html { redirect_to estados_url, notice: 'Estado was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_estado
      @estado = Estado.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def estado_params
      params.require(:estado).permit(:estado, :tipoHabitacion, :precioNoche, :personasHabitacion)
    end
end
