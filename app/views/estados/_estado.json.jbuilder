json.extract! estado, :id, :estado, :tipoHabitacion, :precioNoche, :personasHabitacion, :created_at, :updated_at
json.url estado_url(estado, format: :json)
