json.extract! reserva, :id, :estado_id, :nombreCliente, :cedulaCliente, :celular, :correo, :numeroDias, :totalPagar, :created_at, :updated_at
json.url reserva_url(reserva, format: :json)
