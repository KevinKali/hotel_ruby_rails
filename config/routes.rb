Rails.application.routes.draw do
  resources :reservas
  resources :estados
  get 'liberar', to: 'estados#liberar'
  get 'liberaH', to: 'estados#liberaH'
  get 'buscarReservacion', to: 'reservas#buscar'
  get 'mostrar', to: 'reservas#muestra'
  get 'listarH_todas', to: 'estados#muestra'
  get 'mostrarDisp', to: 'estados#mostrarDisp'
  get 'mostrarReservas', to: 'reservas#listar'
  get 'fotos', to: 'estados#fotos'

  root :to => 'welcome#index'
end
