class CreateReservas < ActiveRecord::Migration[6.0]
  def change
    create_table :reservas do |t|
      t.references :estado, null: false, foreign_key: true
      t.string :nombreCliente
      t.string :cedulaCliente
      t.integer :celular
      t.string :correo
      t.integer :numeroDias
      t.float :totalPagar

      t.timestamps
    end
  end
end
