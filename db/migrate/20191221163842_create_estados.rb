class CreateEstados < ActiveRecord::Migration[6.0]
  def change
    create_table :estados do |t|
      t.integer :estado
      t.string :tipoHabitacion
      t.float :precioNoche
      t.integer :personasHabitacion

      t.timestamps
    end
  end
end
