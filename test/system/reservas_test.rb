require "application_system_test_case"

class ReservasTest < ApplicationSystemTestCase
  setup do
    @reserva = reservas(:one)
  end

  test "visiting the index" do
    visit reservas_url
    assert_selector "h1", text: "Reservas"
  end

  test "creating a Reserva" do
    visit reservas_url
    click_on "New Reserva"

    fill_in "Cedulacliente", with: @reserva.cedulaCliente
    fill_in "Celular", with: @reserva.celular
    fill_in "Correo", with: @reserva.correo
    fill_in "Estado", with: @reserva.estado_id
    fill_in "Nombrecliente", with: @reserva.nombreCliente
    fill_in "Numerodias", with: @reserva.numeroDias
    fill_in "Totalpagar", with: @reserva.totalPagar
    click_on "Create Reserva"

    assert_text "Reserva was successfully created"
    click_on "Back"
  end

  test "updating a Reserva" do
    visit reservas_url
    click_on "Edit", match: :first

    fill_in "Cedulacliente", with: @reserva.cedulaCliente
    fill_in "Celular", with: @reserva.celular
    fill_in "Correo", with: @reserva.correo
    fill_in "Estado", with: @reserva.estado_id
    fill_in "Nombrecliente", with: @reserva.nombreCliente
    fill_in "Numerodias", with: @reserva.numeroDias
    fill_in "Totalpagar", with: @reserva.totalPagar
    click_on "Update Reserva"

    assert_text "Reserva was successfully updated"
    click_on "Back"
  end

  test "destroying a Reserva" do
    visit reservas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Reserva was successfully destroyed"
  end
end
